internal class Program
{
    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        builder.Services.AddControllers();
        builder.Services.AddSwaggerGen();
        builder.Services.AddSpaStaticFiles(configuration =>
        {
            configuration.RootPath = "ClientApp";
        });

        var app = builder.Build();
        app.UseSpaStaticFiles();

        app.UseDefaultFiles();

        if (app.Environment.IsDevelopment())
        {
            app.UseSwagger();
            app.UseSwaggerUI();
        }

        app.UseSpa(spa =>
        {
            spa.Options.SourcePath = "ClientApp";
        });
        app.UseAuthorization();
        app.MapControllers();

        app.Run();
    }
}